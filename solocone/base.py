from django.db import models


class Source(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        abstract = True


class BackupStorage(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        abstract = True


class Destination(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        abstract = True
