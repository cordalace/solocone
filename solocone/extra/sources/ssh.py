import paramiko
from django.db import models

from solocone.base import Source


class SSHSource(Source):
    host = models.CharField(max_length=200)
    user = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

    def get_backup_file(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(
            self.host,
            username=self.user,
            password=self.password,
            port=22,
        )
        stdin, stdout, stderr = ssh.exec_command(
            'tar -cf /tmp/backup.tar /etc',
        )
        while not stdout.closed:
            yield stdout.recv(_READ_BUFFER)
        if stdout.channel.recv_exit_status() != 0:
            raise RuntimeError(
                u'Failed: %(error)s' % {
                    'error': stderr.read(),
                }
            )
        ssh.close()
