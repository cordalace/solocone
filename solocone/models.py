from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=200)


class Backup(models.Model):
    RUNNING = 'RU'
    SUCCESS = 'SU'
    FAILED = 'FA'
    BACKUP_STATUS = (
        (RUNNING, 'Running'),
        (SUCCESS, 'Successful'),
        (FAILED, 'Failed'),

    )
    project = models.ForeignKey(Project)
    status = models.CharField(
        max_length=2,
        choices=BACKUP_STATUS,
        default=RUNNING,
    )
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

from solocone.extra.sources.ssh import SSHSource
